package com.nexsoft.perpustakaan.controller;

import com.nexsoft.perpustakaan.entity.Buku;
import com.nexsoft.perpustakaan.service.BukuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BukuController {
    @Autowired
    private BukuService service;

    @PostMapping("/addBuku")
    public Buku addBuku(@RequestBody Buku buku){
        return service.saveBuku(buku);
    }

    @GetMapping("/bukus")
    public List<Buku> findAllBukus(){
        return (List<Buku>) service.getBukus();
    }

    @GetMapping("/bukuById/{id}")
    public Buku findBukuById(@PathVariable int id){
        return service.getBukuByIdBuku(id);
    }

    @GetMapping("/bukuByJudulBuku/{judulBuku}")
    public Buku findBukuByJudulBuku(@PathVariable String judulBuku){
        return service.getBukuByJudulBuku(judulBuku);
    }
    @GetMapping("/bukuByPengarang/{pengarang}")
    public Buku findBukuByPengarang(@PathVariable String pengarang){
        return service.getBukuByPengarang(pengarang);
    }

    @PutMapping("/updateBuku/{id}")
    public void updateBuku(@RequestBody Buku buku, @PathVariable int id) {
        buku.setId(id);
        service.saveBuku(buku);
    }
    @DeleteMapping("/deleteBuku/{id}")
    public String deleteBuku(@PathVariable int id){
        return service.deleteBuku(id);
        }
    }

