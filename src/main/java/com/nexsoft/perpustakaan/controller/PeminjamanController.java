package com.nexsoft.perpustakaan.controller;

import com.nexsoft.perpustakaan.entity.Member;
import com.nexsoft.perpustakaan.entity.Peminjaman;
import com.nexsoft.perpustakaan.service.PeminjamanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PeminjamanController {
    @Autowired
    private PeminjamanService peminjamanService;

    @PostMapping("/addPinjam")
    public Peminjaman addPinjam(@RequestBody Peminjaman peminjaman){
        return peminjamanService.savePinjam(peminjaman);
    }


    @GetMapping("/pinjams")
    public List<Peminjaman> findAllPinjams(){
        return (List<Peminjaman>) peminjamanService.getPinjams();
    }

//    @GetMapping("/pinjamById/{id}")
//    public Member findPeminjamanById(@PathVariable int idPeminjaman){
//        return peminjamanService.getPeminjamanByIdPeminjam(id);
//    }

    @GetMapping("/memberByTanggalPinjam/{namaMember}")
    public Peminjaman findPeminjamanByTanggalPinjam(@PathVariable String tanggalPinjam){
        return peminjamanService.getPeminjamanByTanggalPinjam(tanggalPinjam);
    }

    @PutMapping("/updatePinjam/{id}")
    public void updatePinjam(@RequestBody Peminjaman peminjaman, @PathVariable int id) {
        peminjaman.setIdPeminjaman(id);
        peminjamanService.savePinjam(peminjaman);
    }
    @DeleteMapping("/deletePeminjaman/{id}")
    public String deletePeminjaman(@PathVariable int id){
        return peminjamanService.deletePeminjaman(id);
    }
}
