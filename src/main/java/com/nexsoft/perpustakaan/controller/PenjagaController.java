package com.nexsoft.perpustakaan.controller;

import com.nexsoft.perpustakaan.entity.Penjaga;
import com.nexsoft.perpustakaan.service.PenjagaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PenjagaController {
    @Autowired
    private PenjagaService penjagaService;

    @PostMapping("/addPenjaga")
    public Penjaga addPenjaga(@RequestBody Penjaga penjaga){
            return penjagaService.savePenjaga(penjaga);
        }


    @GetMapping("/penjagas")
    public List<Penjaga> findAllPenjagas(){
        return (List<Penjaga>) penjagaService.getPenjagas();
        }

        @GetMapping("/penjagaById/{id}")
        public Penjaga findPenjagaById(@PathVariable int id){
            return penjagaService.getPenjagaById(id);
        }

        @GetMapping("/penjagaByNama/{namaPenjaga}")
        public Penjaga findPenjagaByNamaPenjaga(@PathVariable String namaPenjaga){
            return penjagaService.getPenjagaByNama(namaPenjaga);
        }

        @PutMapping("/updatePenjaga/{id}")
        public void updatePenjaga(@RequestBody Penjaga penjaga, @PathVariable int id) {
            penjaga.setIdPenjaga(id);
            penjagaService.savePenjaga(penjaga);
        }
        @DeleteMapping("/deletePenjaga/{id}")
        public String deletePenjaga(@PathVariable int id){
            return penjagaService.deletePenjaga(id);
        }
}
