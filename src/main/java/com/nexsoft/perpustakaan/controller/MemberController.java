package com.nexsoft.perpustakaan.controller;

import com.nexsoft.perpustakaan.entity.Buku;
import com.nexsoft.perpustakaan.entity.Member;
import com.nexsoft.perpustakaan.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @PostMapping("/addMember")
    public Member addMember(@RequestBody Member member){
        return memberService.saveMember(member);
    }


    @GetMapping("/members")
    public List<Member> findAllMembers(){
        return (List<Member>) memberService.getMembers();
    }

    @GetMapping("/memberById/{id}")
    public Member findMemberById(@PathVariable int id){
        return memberService.getMemberByIdMember(id);
    }

    @GetMapping("/memberByNama/{namaMember}")
    public Member findMemberByNamaMember(@PathVariable String namaMember){
        return memberService.getMemberByNama(namaMember);
    }

    @PutMapping("/updateMember/{id}")
    public void updateMember(@RequestBody Member member, @PathVariable int id) {
        member.setId(id);
        memberService.saveMember(member);
    }
    @DeleteMapping("/deleteMember/{id}")
    public String deleteMember(@PathVariable int id){
        return memberService.deleteMember(id);
    }
}


