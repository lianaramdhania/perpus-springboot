package com.nexsoft.perpustakaan.repository;

import com.nexsoft.perpustakaan.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Integer> {
    Member findByNamaMember(String namaMember);
}
