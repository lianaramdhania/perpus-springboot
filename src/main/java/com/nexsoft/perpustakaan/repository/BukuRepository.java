package com.nexsoft.perpustakaan.repository;

import com.nexsoft.perpustakaan.entity.Buku;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BukuRepository extends JpaRepository<Buku, Integer> {
    Buku findByJudulBuku(String judulBuku);
    Buku findByPengarang(String pengarang);
}
