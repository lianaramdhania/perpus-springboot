package com.nexsoft.perpustakaan.repository;

import com.nexsoft.perpustakaan.entity.Peminjaman;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeminjamanRepository extends JpaRepository<Peminjaman, Integer> {
    Peminjaman findByTanggalPinjam(String tanggalPinjam);
}
