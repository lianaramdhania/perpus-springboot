package com.nexsoft.perpustakaan.repository;

import com.nexsoft.perpustakaan.entity.Penjaga;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PenjagaRepository extends JpaRepository<Penjaga, Integer> {
    Penjaga findByNamaPenjaga(String namaPenjaga);
}
