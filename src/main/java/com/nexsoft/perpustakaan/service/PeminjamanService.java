package com.nexsoft.perpustakaan.service;

import com.nexsoft.perpustakaan.entity.Member;
import com.nexsoft.perpustakaan.entity.Peminjaman;
import com.nexsoft.perpustakaan.repository.MemberRepository;
import com.nexsoft.perpustakaan.repository.PeminjamanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeminjamanService {
    @Autowired
    private PeminjamanRepository peminjamanRepository;

    public Peminjaman savePinjam(Peminjaman peminjaman) {
        return peminjamanRepository.save(peminjaman);
    }


    public List<Peminjaman> getPinjams(){
        return peminjamanRepository.findAll();
    }

    public Peminjaman getListByIdPeminjaman(int idPeminjaman) {
        return peminjamanRepository.findById(idPeminjaman).orElse(null);
    }


    public Peminjaman getPeminjamanByTanggalPinjam(String tanggalPinjam) {
        return peminjamanRepository.findByTanggalPinjam(tanggalPinjam);
    }


    public String deletePeminjaman(int idPeminjaman){
        peminjamanRepository.deleteById(idPeminjaman);
        return "Peminjaman Selesai!!";
    }

    public Peminjaman updatePeminjaman(Peminjaman peminjaman){
        Peminjaman existingPeminjaman = peminjamanRepository.findById(peminjaman.getIdPeminjaman()).orElse(null);
        existingPeminjaman.setMember(peminjaman.getMember());
        existingPeminjaman.setBuku(peminjaman.getBuku());
        existingPeminjaman.setTanggalPinjam(peminjaman.getTanggalPinjam());
        existingPeminjaman.setTanggalKembali(peminjaman.getTanggalKembali());
        return peminjamanRepository.save(existingPeminjaman);
    }
}
