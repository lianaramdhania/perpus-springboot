package com.nexsoft.perpustakaan.service;

import com.nexsoft.perpustakaan.entity.Buku;
import com.nexsoft.perpustakaan.repository.BukuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BukuService {
    @Autowired
    private BukuRepository bukuRepository;

    public Buku saveBuku(Buku buku) {
        return bukuRepository.save(buku);
    }

    public List<Buku> getBukus(){
        return bukuRepository.findAll();
    }

    public Buku getBukuByIdBuku(int idBuku) {
        return bukuRepository.findById(idBuku).orElse(null);
    }

    public Buku getBukuByJudulBuku(String judulBuku) {
        return bukuRepository.findByJudulBuku(judulBuku);
    }

    public Buku getBukuByPengarang(String pengarang) {
        return bukuRepository.findByPengarang(pengarang);
    }

    public String deleteBuku(int id){
        bukuRepository.deleteById(id);
        return "Buku removed!!";
    }

    public Buku updateBuku(Buku buku){
        Buku existingBuku = bukuRepository.findById(buku.getIdBuku()).orElse(null);
        existingBuku.setJudulBuku(buku.getJudulBuku());
        existingBuku.setPengarang(buku.getPengarang());
        existingBuku.setPenerbit(buku.getPenerbit());
        existingBuku.setTahunTerbit(buku.getTahunTerbit());
        return bukuRepository.save(existingBuku);
    }
}

