package com.nexsoft.perpustakaan.service;

import com.nexsoft.perpustakaan.entity.Buku;
import com.nexsoft.perpustakaan.entity.Member;
import com.nexsoft.perpustakaan.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberService {
    @Autowired
    private MemberRepository memberRepository;

    public Member saveMember(Member member) {
        return memberRepository.save(member);
    }


    public List<Member> getMembers(){
        return memberRepository.findAll();
    }

    public Member getMemberByIdMember(int idMember) {
        return memberRepository.findById(idMember).orElse(null);
    }


    public Member getMemberByNama(String namaMember) {
        return memberRepository.findByNamaMember(namaMember);
    }


    public String deleteMember(int idMember){
        memberRepository.deleteById(idMember);
        return "Member removed!!";
    }

    public Member updateMember(Member member){
        Member existingMember = memberRepository.findById(member.getIdMember()).orElse(null);
        existingMember.setNamaMember(member.getNamaMember());
        existingMember.setAlamat(member.getAlamat());
        existingMember.setAlamat(member.getNoHp());
        return memberRepository.save(existingMember);
    }
}
