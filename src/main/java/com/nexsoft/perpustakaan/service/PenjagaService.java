package com.nexsoft.perpustakaan.service;

import com.nexsoft.perpustakaan.entity.Penjaga;
import com.nexsoft.perpustakaan.repository.PenjagaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PenjagaService {
    @Autowired
    private PenjagaRepository penjagaRepository;

    public Penjaga savePenjaga(Penjaga penjaga) {
        return penjagaRepository.save(penjaga);
    }


    public List<Penjaga> getPenjagas(){
        return penjagaRepository.findAll();
    }

    public Penjaga getPenjagaById(int idPenjaga) {
        return penjagaRepository.findById(idPenjaga).orElse(null);
    }


    public Penjaga getPenjagaByNama(String namaPenjaga) {
        return penjagaRepository.findByNamaPenjaga(namaPenjaga);
    }


    public String deletePenjaga(int idPenjaga){
        penjagaRepository.deleteById(idPenjaga);
        return "Penjaga ini Sudah Tidak Bertugas!!";
    }

    public Penjaga updatePenjaga(Penjaga penjaga){
        Penjaga existingPenjaga = penjagaRepository.findById(penjaga.getIdPenjaga()).orElse(null);
        existingPenjaga.setNamaPenjaga(penjaga.getNamaPenjaga());
        existingPenjaga.setPassword(penjaga.getPassword());
        return penjagaRepository.save(existingPenjaga);
    }
}
