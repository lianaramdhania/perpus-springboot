package com.nexsoft.perpustakaan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "datapenjaga")
public class Penjaga {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int idPenjaga;

    @Column(nullable = false,length = 30)
    private String namaPenjaga;

    @Column(nullable = false, length = 8)
    private String password;

    public int getIdPenjaga() {
        return idPenjaga;
    }

    public void setIdPenjaga(int idPenjaga) {
        this.idPenjaga = idPenjaga;
    }

    public String getNamaPenjaga() {
        return namaPenjaga;
    }

    public void setNamaPenjaga(String namaPenjaga) {
        this.namaPenjaga = namaPenjaga;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
